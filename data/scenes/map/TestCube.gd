extends RigidBody

const BASE_BULLET_BOOST = 9;

func _ready():
	pass

#func bullet_hit(damage, bullet_global_trans):
#	var direction_vect = bullet_global_trans.basis.z.normalized() * BASE_BULLET_BOOST;
#	apply_impulse((bullet_global_trans.origin - global_transform.origin).normalized(), direction_vect * damage)

func bullet_hit(damage, bullet_hit_pos):
	var direction_vect = global_transform.origin - bullet_hit_pos
	direction_vect = direction_vect.normalized()
	print("Hit!")
	apply_impulse(bullet_hit_pos, direction_vect * damage *0.25)
	
	
