extends KinematicBody

const GRAVITY = -24.8
var vel = Vector3()
const MAX_SPEED = 10
const SPRINT_MOVE_SPEED = 8
const SPRINT_STAMINA_MAX = 17   
const SPRINT_STAMINA_CONSUME_RATE = 10
const SPRINT_STAMINA_RESTORE_RATE = 10
const STAND_MOVE_SPEED = 3
const CROUCH_MOVE_SPEED = 1
const CRAWL_MOVE_SPEED = 0.2
const JUMP_SPEED = 8
const CROUCH_TO_STANDUP_SPEED = 2
const STANDUP_TO_CROUCH_SPEED = 2
const CRAWL_TO_CROUCH_SPEED = 1
const CROUCH_TO_CRAWL_SPEED = 1
const CROUCH_SPEED = 2
const CRAWL_SPEED = 1
const ACCEL = 2.5

const DEFAULT_HEIGHT = 1.0
const CROUCH_HEIGHT = 0.1
const CRAWL_HEIGHT = 0.0
const DEFAULT_RADIUS = 0.3
const CRAWL_RADIUS = 0.1

const CROUCH_EYE_ADJUSTMENT = 0.01
const CRAWL_EYE_ADJUSTMENT = 0.008

const LEAN_ANGLE = 30	# degree
const LEAN_SPEED = 1	# radians
const LEAN_POSCOMP_SPEED = 0.6	# compensation for the base position 

var dir = Vector3()

const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

var movement_linear_speed
var is_sprint
var sprint_stamina
var stamina_consume_rate
var stamina_restore_rate

var camera
var rotation_helper
var body_collision_shape
var ceiling_checker

var MOUSE_SENSITIVITY = 0.05
var INVERT_MOUSE = -1    # -1 means invert mouse look, 1 means normal mouse look

var ceiling_hit
var ceiling_collision_point = Vector3()
var rotation_helper_point = Vector3()
var body_point = Vector3()
var head_ceiling_distance

var stance_state    # 0 = standing, 1 = crounching, 2 = crawling, 9XY = X-Y transistion, 99 = error
var lean_state      # 0 = center, 1 = lean left, 2 = lean right, 7XY = X-Y transition, 99 error


var current_weapon_name = "UNARMED"
var weapons = {"UNARMED":null, "PISTOL":null, "RIFLE":null}
const WEAPON_NUMBER_TO_NAME = {0:"UNARMED", 1:"PISTOL", 2:"RIFLE"}
const WEAPON_NAME_TO_NUMBER = {"UNARMED":0, "PISTOL":1, "RIFLE":2}
var changing_weapon = false
var changing_weapon_name = "PISTOL"
var reloading_weapon = false
var health = 100

var UI_status_label

func _ready():
	print("player:", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)
	camera = $Rotation_Helper/Camera
	rotation_helper = $Rotation_Helper
	body_collision_shape = $Body_CollisionShape
	ceiling_checker = $CeilingChecker
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	stance_state = 0
	lean_state = 0
	movement_linear_speed = STAND_MOVE_SPEED
	is_sprint = false
	sprint_stamina = SPRINT_STAMINA_MAX
	stamina_consume_rate = SPRINT_STAMINA_CONSUME_RATE
	stamina_restore_rate = SPRINT_STAMINA_RESTORE_RATE
	
	weapons["PISTOL"] = $Rotation_Helper/Gun_Fire_Points/Gun_00_Point/Gun_00
	weapons["RIFLE"] = $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01
	
	var gun_aim_point_pos = $Rotation_Helper/Gun_Aim_Points.global_transform.origin
	
	print("player: before rotate", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)
	
	for weapon in weapons:
		var weapon_node = weapons[weapon]
		if weapon_node != null:
			weapon_node.player_node = self
			weapon_node.look_at(gun_aim_point_pos, Vector3(0, 1, 0))
			print("player: after lookat", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)
#			weapon_node.rotate_object_local(Vector3(0, 1, 0), deg2rad(90))

	print("player: after rotate", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)
	current_weapon_name = "UNARMED"
	changing_weapon_name = "UNARMED"
#	print("player:", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)
	
func _physics_process(delta):
	process_input(delta)
	process_movement(delta)
	process_changing_weapons(delta)
#	print("player:", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)

func process_input(delta):
	# ----------------------------------
	# Walking
	dir = Vector3()
	var cam_xform = camera.get_global_transform()

	var input_movement_vector = Vector2()

	if Input.is_action_pressed("movement_forward"):
		input_movement_vector.y += 1
	if Input.is_action_pressed("movement_backward"):
		input_movement_vector.y -= 1
	if Input.is_action_pressed("movement_left"):
		input_movement_vector.x -= 1
	if Input.is_action_pressed("movement_right"):
		input_movement_vector.x += 1

	input_movement_vector = input_movement_vector.normalized()

	# Basis vectors are already normalized.
	dir += -cam_xform.basis.z * input_movement_vector.y
	dir += cam_xform.basis.x * input_movement_vector.x
	# ----------------------------------

	# ----------------------------------
	# Sprint
	if Input.is_action_pressed("movement_sprint"):
		if((stance_state == 0) && (sprint_stamina > 0)):     # stand state
			is_sprint = true
			movement_linear_speed = SPRINT_MOVE_SPEED
		else:
			is_sprint = false
			if(stance_state == 0):
				movement_linear_speed = STAND_MOVE_SPEED
			elif((stance_state == 1) || (stance_state == 901) || (stance_state == 10)):
				movement_linear_speed = CROUCH_MOVE_SPEED
			elif((stance_state == 2) || (stance_state == 912) || (stance_state == 21)):
				movement_linear_speed = CRAWL_MOVE_SPEED
	else:
		is_sprint = false
		if(stance_state == 0):     # stand state
			movement_linear_speed = STAND_MOVE_SPEED
		elif((stance_state == 1) || (stance_state == 901) || (stance_state == 10)):
			movement_linear_speed = CROUCH_MOVE_SPEED
		elif((stance_state == 2) || (stance_state == 912) || (stance_state == 21)):
			movement_linear_speed = CRAWL_MOVE_SPEED
	#-----------------------------------
	
	# ----------------------------------
	# Jumping
	if is_on_floor():
		if Input.is_action_just_pressed("movement_jump"):
			# ----------------------------------
			# check overhead obstacles
			ceiling_hit = false
			body_point = $".".translation
			if ceiling_checker.is_colliding():
				ceiling_collision_point = ceiling_checker.get_collision_point()
				head_ceiling_distance = abs(ceiling_collision_point.distance_to(body_point)) - (body_collision_shape.shape.height/2)
				if(stance_state == 0):     # stand state
					if(head_ceiling_distance < 0.58):
						ceiling_hit = true
				elif(stance_state == 1):    # crouch state
					if(head_ceiling_distance < 0.58):
						ceiling_hit = true
				elif(stance_state == 2):    # crawl state
					if(head_ceiling_distance < 0.58):
						ceiling_hit = true
				else:    # trasition states
					if(head_ceiling_distance < 0.58):
						ceiling_hit = true
			#print(head_ceiling_distance)    #debug purpose
			#-----------------------------------
			if not ceiling_hit:
				if stance_state == 0:
					vel.y = JUMP_SPEED
				elif stance_state == 1:
					body_collision_shape.shape.height += CROUCH_TO_STANDUP_SPEED * delta
					body_collision_shape.shape.height = clamp(body_collision_shape.shape.height, CROUCH_HEIGHT, DEFAULT_HEIGHT)	
					stance_state = 10
				elif stance_state == 2:
					body_collision_shape.shape.height += CRAWL_TO_CROUCH_SPEED * delta
					body_collision_shape.shape.height = clamp(body_collision_shape.shape.height, CRAWL_HEIGHT, DEFAULT_HEIGHT)	
					stance_state = 21
				else:
					stance_state=99
		else:
			if stance_state == 10:
				body_collision_shape.shape.height += CROUCH_TO_STANDUP_SPEED * delta
				rotation_helper.translate(Vector3(0, CROUCH_EYE_ADJUSTMENT, 0))
				if body_collision_shape.shape.height > DEFAULT_HEIGHT:
					body_collision_shape.shape.height = DEFAULT_HEIGHT
					movement_linear_speed = STAND_MOVE_SPEED
					stance_state = 0
			elif stance_state == 21:
				body_collision_shape.shape.height += CRAWL_TO_CROUCH_SPEED * delta
				body_collision_shape.shape.radius += CRAWL_TO_CROUCH_SPEED * delta
				rotation_helper.translate(Vector3(0, CRAWL_EYE_ADJUSTMENT, 0))
				if body_collision_shape.shape.height > CROUCH_HEIGHT:
					body_collision_shape.shape.height = CROUCH_HEIGHT
					body_collision_shape.shape.radius = DEFAULT_RADIUS
					movement_linear_speed = CROUCH_MOVE_SPEED
					stance_state = 1
	# ----------------------------------

	# ----------------------------------
	# Crouching
		
	if is_on_floor():
		if Input.is_action_just_pressed("movement_crouch"):
			if stance_state == 0:
				body_collision_shape.shape.height -= STANDUP_TO_CROUCH_SPEED * delta
				body_collision_shape.shape.height = clamp(body_collision_shape.shape.height, CROUCH_HEIGHT, DEFAULT_HEIGHT)	
				movement_linear_speed = CROUCH_MOVE_SPEED
				stance_state = 901
			elif stance_state == 1:
				body_collision_shape.shape.height -= CROUCH_TO_CRAWL_SPEED * delta
				body_collision_shape.shape.height = clamp(body_collision_shape.shape.height, CROUCH_HEIGHT, DEFAULT_HEIGHT)	
				movement_linear_speed = CRAWL_MOVE_SPEED
				stance_state = 912
		else:
			if stance_state == 901:
				body_collision_shape.shape.height -= STANDUP_TO_CROUCH_SPEED * delta
				rotation_helper.translate(Vector3(0, -CROUCH_EYE_ADJUSTMENT, 0))
				movement_linear_speed = CROUCH_MOVE_SPEED
				if body_collision_shape.shape.height < CROUCH_HEIGHT:
					body_collision_shape.shape.height = CROUCH_HEIGHT
					stance_state = 1
			elif stance_state == 912:
				body_collision_shape.shape.height -= CROUCH_TO_CRAWL_SPEED * delta
				body_collision_shape.shape.radius -= CROUCH_TO_CRAWL_SPEED * delta
				rotation_helper.translate(Vector3(0, -CRAWL_EYE_ADJUSTMENT, 0))
				movement_linear_speed = CRAWL_MOVE_SPEED
				if body_collision_shape.shape.height < CRAWL_HEIGHT:
					body_collision_shape.shape.height = CRAWL_HEIGHT
					body_collision_shape.shape.radius = CRAWL_RADIUS
					stance_state = 2
					

#		else:
#		elif not ceiling_hit:
#			movement_linear_speed = STAND_MOVE_SPEED
#			body_collision_shape.shape.height += CROUCH_SPEED * delta
				
#		body_collision_shape.shape.height = clamp(body_collision_shape.shape.height, CROUCH_HEIGHT, DEFAULT_HEIGHT)	
	# ----------------------------------

	# ----------------------------------
	# Leaning
	if is_on_floor():
#		print(lean_state)
#		print(delta)
#		print($".".rotation_degrees.z)
#		print($".".translation.x)
		if Input.is_action_pressed("movement_lean_right"):
			if stance_state == 0:
				if lean_state == 0:
					$".".rotate_object_local(Vector3(0,0,1), -LEAN_SPEED * delta)
					$".".translate_object_local(Vector3(LEAN_POSCOMP_SPEED * delta, 0, 0))
					lean_state = 702
				if lean_state == 702:
					$".".rotate_object_local(Vector3(0,0,1), -LEAN_SPEED * delta)
					$".".translate_object_local(Vector3(LEAN_POSCOMP_SPEED * delta, 0, 0))
					if($".".rotation_degrees.z < -LEAN_ANGLE):
						$".".rotation_degrees.z = -LEAN_ANGLE
						lean_state = 2
					else:
						lean_state = 702
			elif stance_state == 1:
				if lean_state == 0:
					$".".rotate_object_local(Vector3(0,0,1), -LEAN_SPEED * delta)
					$".".translate_object_local(Vector3(LEAN_POSCOMP_SPEED * delta, 0, 0))
					lean_state = 702
				if lean_state == 702:
					$".".rotate_object_local(Vector3(0,0,1), -LEAN_SPEED * delta)
					$".".translate_object_local(Vector3(LEAN_POSCOMP_SPEED * delta, 0, 0))
					if($".".rotation_degrees.z < -LEAN_ANGLE):
						$".".rotation_degrees.z = -LEAN_ANGLE
						lean_state = 2
					else:
						lean_state = 702
		elif Input.is_action_pressed("movement_lean_left"):
			if stance_state == 0:
				if lean_state == 0:
					$".".rotate_object_local(Vector3(0,0,1),LEAN_SPEED * delta)
					$".".translate_object_local(Vector3(-LEAN_POSCOMP_SPEED * delta, 0, 0))
					lean_state = 701
				if lean_state == 701:
					$".".rotate_object_local(Vector3(0,0,1),LEAN_SPEED * delta)
					$".".translate_object_local(Vector3(-LEAN_POSCOMP_SPEED * delta, 0, 0))
					if($".".rotation_degrees.z > LEAN_ANGLE):
						$".".rotation_degrees.z = LEAN_ANGLE
						lean_state = 1
					else:
						lean_state = 701
			elif stance_state == 1:
				if lean_state == 0:
					$".".rotate_object_local(Vector3(0,0,1),LEAN_SPEED * delta)
					$".".translate_object_local(Vector3(-LEAN_POSCOMP_SPEED * delta, 0, 0))
					lean_state = 701
				if lean_state == 701:
					$".".rotate_object_local(Vector3(0,0,1),LEAN_SPEED * delta)
					$".".translate_object_local(Vector3(-LEAN_POSCOMP_SPEED * delta, 0, 0))
					if($".".rotation_degrees.z > LEAN_ANGLE):
						$".".rotation_degrees.z = LEAN_ANGLE
						lean_state = 1
					else:
						lean_state = 701
		else: # no lean key pressed
			if lean_state == 2:
				$".".rotate_object_local(Vector3(0,0,1), LEAN_SPEED * delta)
				$".".translate_object_local(Vector3(-LEAN_POSCOMP_SPEED * delta, 0, 0))
				if($".".rotation_degrees.z > 0.0):
					$".".rotation_degrees.z = 0.0
					lean_state = 0
				else:
					lean_state = 720
			elif lean_state == 720:
				$".".rotate_object_local(Vector3(0,0,1), LEAN_SPEED * delta)
				$".".translate_object_local(Vector3(-LEAN_POSCOMP_SPEED * delta, 0, 0))
				if($".".rotation_degrees.z > 0.0):
					$".".rotation_degrees.z = 0
					lean_state = 0
				else:
					lean_state = 720
			elif lean_state == 702:
				$".".rotate_object_local(Vector3(0,0,1), LEAN_SPEED * delta)
				$".".translate_object_local(Vector3(-LEAN_POSCOMP_SPEED * delta, 0, 0))
				if($".".rotation_degrees.z > 0.0):
					$".".rotation_degrees.z = 0
					lean_state = 0
				else:
					lean_state = 720
			elif lean_state == 1:
				$".".rotate_object_local(Vector3(0,0,1), -LEAN_SPEED * delta)
				$".".translate_object_local(Vector3(LEAN_POSCOMP_SPEED * delta, 0, 0))
				if($".".rotation_degrees.z < 0.0):
					$".".rotation_degrees.z = 0.0
					lean_state = 0
				else:
					lean_state = 710
			elif lean_state == 710:
				$".".rotate_object_local(Vector3(0,0,1), -LEAN_SPEED * delta)
				$".".translate_object_local(Vector3(LEAN_POSCOMP_SPEED * delta, 0, 0))
				if($".".rotation_degrees.z < 0.0):
					$".".rotation_degrees.z = 0
					lean_state = 0
				else:
					lean_state = 710
			elif lean_state == 701:
				$".".rotate_object_local(Vector3(0,0,1), -LEAN_SPEED * delta)
				$".".translate_object_local(Vector3(LEAN_POSCOMP_SPEED * delta, 0, 0))
				if($".".rotation_degrees.z < 0.0):
					$".".rotation_degrees.z = 0
					lean_state = 0
				else:
					lean_state = 710
	# ----------------------------------
	
	# ----------------------------------
	# Changing weapons.
	var weapon_change_number = WEAPON_NAME_TO_NUMBER[current_weapon_name]
	
	if Input.is_key_pressed(KEY_0):
		weapon_change_number = 0
	if Input.is_key_pressed(KEY_1):
		weapon_change_number = 1
	if Input.is_key_pressed(KEY_2):
		weapon_change_number = 2
	
	if changing_weapon == false:
		if WEAPON_NUMBER_TO_NAME[weapon_change_number] != current_weapon_name:
			changing_weapon_name = WEAPON_NUMBER_TO_NAME[weapon_change_number]
			changing_weapon = true
	# ----------------------------------
	
	# ----------------------------------
	# Firing the weapons
	if Input.is_action_just_pressed("weapon_fire"):
		if changing_weapon == false:
			var current_weapon = weapons[current_weapon_name]
			if(current_weapon != null):
#				print(current_weapon)
				current_weapon.fire_weapon()
	# ----------------------------------
	
	# ----------------------------------
	# Reload current weapon
	if Input.is_action_just_pressed("weapon_reload"):
		if changing_weapon == false:
			var current_weapon = weapons[current_weapon_name]
			if(current_weapon != null):
#				print(current_weapon)
				current_weapon.reload_weapon()

	# ----------------------------------
	
	# ----------------------------------
	# Zooming/Aiming
	if Input.is_action_pressed("zoom"):
		if changing_weapon == false:
			$Rotation_Helper/Camera.fov = 20
		else:
			$Rotation_Helper/Camera.fov = 70
	else:
		$Rotation_Helper/Camera.fov = 70
	# ----------------------------------
	# ----------------------------------
	# Capturing/Freeing the cursor
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	# ----------------------------------

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()

	vel.y += delta * GRAVITY
	var hvel = vel
	hvel.y = 0

	var target = dir
	target *= movement_linear_speed
	
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))
	
	# stamina update
	if((vel.x !=0) || (vel.y !=0)): # player is movig
		if(is_sprint):
			sprint_stamina -= stamina_consume_rate * delta
	else:
		sprint_stamina += stamina_restore_rate * delta
		
	sprint_stamina = clamp(sprint_stamina, 0, SPRINT_STAMINA_MAX)
	#print(sprint_stamina)
	# ----
	
func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotation_helper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * INVERT_MOUSE))

		var camera_rot = rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -70, 70)
		rotation_helper.rotation_degrees = camera_rot

func process_changing_weapons(delta):
	if changing_weapon == true:

		var weapon_unequipped = false
		var current_weapon = weapons[current_weapon_name]

		if current_weapon == null:
			weapon_unequipped = true
		else:
			if current_weapon.is_weapon_enabled == true:
				weapon_unequipped = current_weapon.unequip_weapon()
			else:
				weapon_unequipped = true

		if weapon_unequipped == true:
			var weapon_equiped = false
			var weapon_to_equip = weapons[changing_weapon_name]

			if weapon_to_equip == null:
				weapon_equiped = true
			else:
				if weapon_to_equip.is_weapon_enabled == false:
					weapon_equiped = weapon_to_equip.equip_weapon()
				else:
					weapon_equiped = true

			if weapon_equiped == true:
				changing_weapon = false
				current_weapon_name = changing_weapon_name
				changing_weapon_name = ""
				print(current_weapon_name)
				


func fire_bullet():
	if changing_weapon == true:
		return
		
	weapons[current_weapon_name].fire_weapon()

func process_reloading(delta):
	if reloading_weapon == true:
		var current_weapon = weapons[current_weapon_name]
		if current_weapon != null:
			current_weapon.reload_weapon()
		reloading_weapon = false

func bullet_hit(damage, bullet_hit_pos):
	var direction_vect = global_transform.origin - bullet_hit_pos
	direction_vect = direction_vect.normalized()
	health = health - damage
	print("    Player: Hit! Damage=", damage, " Healt=", health)
#	apply_impulse(bullet_hit_pos, direction_vect * damage *0.25)
