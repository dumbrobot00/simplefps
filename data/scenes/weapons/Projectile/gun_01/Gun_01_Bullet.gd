extends RigidBody

var BULLET_SPEED = 0.005
var BULLET_DAMAGE = 40

const KILL_TIMER = 10
const RICOCHET_DURATION = 0.3
var timer = 0
var recochet_timer = 0

var hit_victim = false
var hit_not_victim = false

func _ready():
#	$Area.connect("body_entered", self, "collided")
	$".".connect("body_entered", self,"_on_Gun_01_Bullet_body_entered")
	print("bullet spawnd")
	pass
	
func _physics_process(delta):
#	var forward_dir = global_transform.basis.z.normalized()
#	$".".global_translate(forward_dir * BULLET_SPEED * delta)
	apply_impulse(transform.basis.z, transform.basis.z * BULLET_SPEED)
	

	$OmniLight.light_energy = 20.0
	timer += delta
	if(timer > 0.05):
		$OmniLight.light_energy = 0.6
	if timer >= KILL_TIMER:
		queue_free()


func collided(body):
	if hit_victim == false:
		if body.has_method("bullet_hit"):
			print("collided")
			body.bullet_hit(BULLET_DAMAGE, self.global_transform.origin)

	hit_victim = true
	queue_free()


func _on_Gun_01_Bullet_body_entered(body):
	if hit_victim == false:
		if body.has_method("bullet_hit"):
			print("bullet entered")
			body.bullet_hit(BULLET_DAMAGE, self.global_transform.origin)
			hit_victim = true
			queue_free()
		else:
#			hit_not_victim = true
			recochet_timer += 0.1
			if(recochet_timer > RICOCHET_DURATION):
				queue_free()
	
	
	
