extends Spatial


const IDLE_ANIM_NAME = "Rifle_idle"
const FIRE_ANIM_NAME = "Rifle_fire"

const INIT_MAG_SIZE = 6 
const MAX_BULLET_SIZE = 10

var mag_count
var bullet_count
var ammo_count


var is_weapon_enabled = false

var bullet_scene = preload("Gun_01_Bullet.tscn")

var player_node = null

func _ready():
	visible = false
	mag_count = INIT_MAG_SIZE
	bullet_count = MAX_BULLET_SIZE
	ammo_count = mag_count * bullet_count
	print("onready ", self.global_transform)
	print("bullet_count=%d | mag_count=%d" % [bullet_count, mag_count])
	pass

func _process(delta):
#	print("process: ", self.global_transform)
	pass
	
func fire_weapon():
	if(bullet_count > 0):
		print("on fire ", self.global_transform)
		var clone = bullet_scene.instance()
		var scene_root = get_tree().root.get_children()[0]
		scene_root.add_child(clone)
		
#		self.add_child(clone)
		
		clone.global_transform = self.global_transform
		print("clone ", clone.global_transform)
#		clone.rotate_y(PI)
#		print("invert: ", clone.global_transform)
		
		bullet_count -= 1
		ammo_count = (mag_count -1) * MAX_BULLET_SIZE + bullet_count
		print("Gun: bullet_count=%d | mag_count=%d" % [bullet_count, mag_count])
	else:
		print("Gun: No bullet left. Reload!")

func reload_weapon():
	if(mag_count > 0):
		mag_count -= 1
		bullet_count = MAX_BULLET_SIZE
		ammo_count = mag_count * MAX_BULLET_SIZE
		print("bullet_count=%d | mag_count=%d" % [bullet_count, mag_count])
	else:
		print("No magazine left. Find magazine or Run")
		
func equip_weapon():
	is_weapon_enabled = true
	visible = true
	return true

func unequip_weapon():
	is_weapon_enabled = false
	visible = false
	return true
