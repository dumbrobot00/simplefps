extends RigidBody

enum {
	IDLE
	ALERT
}

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var state = IDLE
var target

const GRAVITY = -24.8
var vel = Vector3()
const MAX_SPEED = 10
const SPRINT_MOVE_SPEED = 8
const SPRINT_STAMINA_MAX = 17   
const SPRINT_STAMINA_CONSUME_RATE = 10
const SPRINT_STAMINA_RESTORE_RATE = 10
const STAND_MOVE_SPEED = 3
const CROUCH_MOVE_SPEED = 1
const CRAWL_MOVE_SPEED = 0.2
const JUMP_SPEED = 8
const CROUCH_TO_STANDUP_SPEED = 2
const STANDUP_TO_CROUCH_SPEED = 2
const CRAWL_TO_CROUCH_SPEED = 1
const CROUCH_TO_CRAWL_SPEED = 1
const CROUCH_SPEED = 2
const CRAWL_SPEED = 1
const ACCEL = 2.5

const DEFAULT_HEIGHT = 1.0
const CROUCH_HEIGHT = 0.1
const CRAWL_HEIGHT = 0.0
const DEFAULT_RADIUS = 0.3
const CRAWL_RADIUS = 0.1

const CROUCH_EYE_ADJUSTMENT = 0.01
const CRAWL_EYE_ADJUSTMENT = 0.008

const LEAN_ANGLE = 30	# degree
const LEAN_SPEED = 1	# radians
const LEAN_POSCOMP_SPEED = 0.6	# compensation for the base position 

var dir = Vector3()

const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

var movement_linear_speed
var is_sprint
var sprint_stamina
var stamina_consume_rate
var stamina_restore_rate

var camera
var rotation_helper
var body_collision_shape
var ceiling_checker

var MOUSE_SENSITIVITY = 0.05
var INVERT_MOUSE = -1    # -1 means invert mouse look, 1 means normal mouse look

var ceiling_hit
var ceiling_collision_point = Vector3()
var rotation_helper_point = Vector3()
var body_point = Vector3()
var head_ceiling_distance

var stance_state    # 0 = standing, 1 = crounching, 2 = crawling, 9XY = X-Y transistion, 99 = error
var lean_state      # 0 = center, 1 = lean left, 2 = lean right, 7XY = X-Y transition, 99 error

var current_weapon_name = "UNARMED"
var weapons = {"UNARMED":null, "PISTOL":null, "RIFLE":null}
const WEAPON_NUMBER_TO_NAME = {0:"UNARMED", 1:"PISTOL", 2:"RIFLE"}
const WEAPON_NAME_TO_NUMBER = {"UNARMED":0, "PISTOL":1, "RIFLE":2}
var changing_weapon = false
var changing_weapon_name = "PISTOL"
var reloading_weapon = false
var health = 100
const TURN_SPEED = 0.2
const MAX_ROTATE_Y = 0.01
const AIM_FIRE_ERROR_RANGE = 0.0

var aim_ray
var eyes
var eyesvertical
var aim_fire_error

var tgt_in_aim_state = false
var shoottimer

# Called when the node enters the scene tree for the first time.
func _ready():
	camera = $Rotation_Helper/Camera
	rotation_helper = $Rotation_Helper
	body_collision_shape = $Body_CollisionShape
	ceiling_checker = $CeilingChecker
	
	aim_ray = $Rotation_Helper/MeshInstance/Aim_RayCast
	eyes = $Eyes
	eyesvertical = $Rotation_Helper/EyesVertical
	shoottimer = $ShootTimer
	
	stance_state = 0
	lean_state = 0
	movement_linear_speed = STAND_MOVE_SPEED
	is_sprint = false
	sprint_stamina = SPRINT_STAMINA_MAX
	stamina_consume_rate = SPRINT_STAMINA_CONSUME_RATE
	stamina_restore_rate = SPRINT_STAMINA_RESTORE_RATE
	
	weapons["PISTOL"] = $Rotation_Helper/Gun_Fire_Points/Gun_00_Point/Gun_00
	weapons["RIFLE"] = $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01
	
	var gun_aim_point_pos = $Rotation_Helper/Gun_Aim_Points.global_transform.origin

	print("bot1: before rotate", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)

	for weapon in weapons:
		var weapon_node = weapons[weapon]
		if weapon_node != null:
			weapon_node.player_node = self
			weapon_node.look_at(gun_aim_point_pos, Vector3(0, 1, 0))
			print("bot1: after lookat", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)
#			weapon_node.rotate_object_local(Vector3(0, 1, 0), deg2rad(90))

	print("bot1: after rotate", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)
	current_weapon_name = "UNARMED"
	changing_weapon_name = "UNARMED"
	
	_action_change_weapon("RIFLE")
	print("Bot:", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)

func _on_SightRange_body_entered(body):
	print("    NPC: body entered")
	if body.is_in_group("Player"):
		state = ALERT
		target = body
		print("player entered")


func _on_SightRange_body_exited(body):
	print("    NPC: body exit")
	if body.is_in_group("Player"):
		state = IDLE
		print("Player exit")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
#	print("delta")
#	print(state)
	match state:
		IDLE:
#			print("IDLE state")
			shoottimer.stop()
			pass
		ALERT:
#			print("    NPC: ALERT state")
			eyes.look_at(target.global_transform.origin, Vector3.UP)
			var eyes_rotate_y = eyes.rotation.y
#			print(eyes_rotate_y)
			
			eyesvertical.look_at((target.global_transform.origin + Vector3(0, 0.5, 0)), Vector3.LEFT)
			var eyes_rotate_x = eyesvertical.rotation.x
#			print(eyes_rotate_x)
			
			# target seen in the front sight region
			if(eyes_rotate_y < 1.0 and eyes_rotate_y > -1.0):
				if(eyes_rotate_x < 1.0 and eyes_rotate_x > -1.0):
#					print("    NPC: rotating and tracking target")

					if eyes_rotate_y > 0:
						if eyes_rotate_y < MAX_ROTATE_Y:
							rotate_y(eyes_rotate_y )
						else:
							rotate_y(MAX_ROTATE_Y)
					else:
						if eyes_rotate_y > (-1 * MAX_ROTATE_Y):
							rotate_y(eyes_rotate_y)
						else:
							rotate_y(-1 * MAX_ROTATE_Y)

					#rotate_y(eyes_rotate_y * TURN_SPEED)
					rotation_helper.rotate_x(eyes_rotate_x * TURN_SPEED)
					
#					print(eyes_rotate_y)
					if(eyes_rotate_y < 0.02 and eyes_rotate_y > -0.02):
						if aim_ray.is_colliding():
#							print("Ray colliding")
							var tgt_in_aim = aim_ray.get_collider()
							if tgt_in_aim.is_in_group("Player"):
								if tgt_in_aim_state == false:
									shoottimer.start()
#									print("    NPC: shoot timer_start")
									tgt_in_aim_state = true
#								print("    NPC: target in aim")
						
							else:
								shoottimer.stop()
								tgt_in_aim_state = false
								print("    NPC: target out of aim")
							
	process_changing_weapons(delta)
#	print("Bot:", $Rotation_Helper/Gun_Fire_Points/Gun_01_Point/Gun_01.global_transform)


func _on_ShootTimer_timeout():
			print("    NPC: Firing!")
			_action_fire_weapon()
			tgt_in_aim_state = false
# ----------------------------------
	# Firing the weapons
func _action_fire_weapon():
	if changing_weapon == false:
		var current_weapon = weapons[current_weapon_name]
		if(current_weapon != null):
			print("_action_fire_weapon:", current_weapon_name)
#			aim_fire_error = rand_range(-AIM_FIRE_ERROR_RANGE, AIM_FIRE_ERROR_RANGE)
#			print(aim_fire_error)
#			rotate_y(aim_fire_error)
			current_weapon.fire_weapon()
# ----------------------------------
	
# ----------------------------------
# Reload current weapon
func _action_reload_weapon():
	if changing_weapon == false:
		var current_weapon = weapons[current_weapon_name]
		if(current_weapon != null):
#			print(current_weapon)
			current_weapon.reload_weapon()
# ----------------------------------

# ----------------------------------
	# Changing weapons.
func _action_change_weapon(change_weapon_name):
	var current_weapon_number = WEAPON_NAME_TO_NUMBER[current_weapon_name]
	var change_weapon_number = WEAPON_NAME_TO_NUMBER[change_weapon_name] 
	current_weapon_number = change_weapon_number

	if changing_weapon == false:
		if WEAPON_NUMBER_TO_NAME[current_weapon_number] != current_weapon_name:
			changing_weapon_name = WEAPON_NUMBER_TO_NAME[current_weapon_number]
			changing_weapon = true
# ----------------------------------

# ----------------------------------
func process_changing_weapons(delta):
	if changing_weapon == true:

		var weapon_unequipped = false
		var current_weapon = weapons[current_weapon_name]

		if current_weapon == null:
			weapon_unequipped = true
		else:
			if current_weapon.is_weapon_enabled == true:
				weapon_unequipped = current_weapon.unequip_weapon()
			else:
				weapon_unequipped = true

		if weapon_unequipped == true:
			var weapon_equiped = false
			var weapon_to_equip = weapons[changing_weapon_name]

			if weapon_to_equip == null:
				weapon_equiped = true
			else:
				if weapon_to_equip.is_weapon_enabled == false:
					weapon_equiped = weapon_to_equip.equip_weapon()
				else:
					weapon_equiped = true

			if weapon_equiped == true:
				changing_weapon = false
				current_weapon_name = changing_weapon_name
				changing_weapon_name = ""
				print(current_weapon_name)
# -----------------------------------


func process_reloading(delta):
	if reloading_weapon == true:
		var current_weapon = weapons[current_weapon_name]
		if current_weapon != null:
			current_weapon.reload_weapon()
		reloading_weapon = false

func bullet_hit(damage, bullet_hit_pos):
	var direction_vect = global_transform.origin - bullet_hit_pos
	direction_vect = direction_vect.normalized()
	health = health - damage
	print("    NPC: Hit! Damage=", damage, " Health=", health)
	apply_impulse(bullet_hit_pos, direction_vect * damage *0.25)

